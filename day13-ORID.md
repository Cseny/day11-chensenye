# O

- Use router to jump to the page.
- Use axios to obtain backend data.


# R

This is a fruitful day.

# I

- The use of ant design is still relatively unfamiliar

# D

- Introducing react router dom in index.js can help us create routes and obtain browser paths to jump to corresponding components.
- Axios can be used to asynchronously obtain backend data.

