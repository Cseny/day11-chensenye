# O

- Reviewed spring through context maps
- React


# R

This is a fruitful day.

# I

- I have not studied React before and have gained a good understanding of the basics of React through the course.

# D

- React can only be passed from a parent component to a child component, and cannot be passed in both directions.
- When a sibling component needs to pass a value, the value needs to be defined in the parent component and passed through the parent component as a mediator.