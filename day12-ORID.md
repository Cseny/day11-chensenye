# O

- Learn Redux
- Refactor TodoList with Redux


# R

This is a fruitful day.

# I

- I am not yet familiar with CSS usage

# D

- Redux manages global attributes by creating a store, and all components can modify the attributes in the store through write time copying, as well as read the attribute values in the store. Redux facilitates communication between components.

