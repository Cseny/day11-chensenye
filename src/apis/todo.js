import api from "./api";

export const getTodoTasks = () => {
    return api.get('/todoList')
}

export const createTodoTasks = (todoTask) => {
    return api.post('/todoList', todoTask)
}

export const updateTodoTask = (id, todoTask) => {
    return api.put(`/todoList/${id}`, todoTask);
}

export const deleteTodoTask = (id) => {
    return api.delete(`/todoList/${id}`);
}