import {useState} from "react";
import './inputTodo.css'
import {useTodos} from "./hooks/useTodo";
import {Button, Input} from "antd";

const InputTodo = () => {
    const {createTodo} = useTodos();
    const [inputValue, setInputValue] = useState('');
    const add = async () => {
        await createTodo(inputValue, setInputValue);
    };

    const handleInputChange = (event) => {
        setInputValue(event.target.value);
    };

    return (
        <div>
            <Input className="inputTask" type="text" value={inputValue} onChange={handleInputChange}/>
            <Button type="primary" onClick={add}>Add</Button>
        </div>);
}

export default InputTodo;