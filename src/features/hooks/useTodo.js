import * as todoApi from "../../apis/todo";
import {initTodoTasks} from "../todoListSlice";
import {useDispatch} from "react-redux";

export const useTodos = () => {
    const dispatch = useDispatch();

    async function loadTodos() {
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
    }

    async function updateTodo(task) {
        await todoApi.updateTodoTask(task.id, {done: !task.done});
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
    }

    async function updateTaskText(id, text) {
        await todoApi.updateTodoTask(id, {text: text});
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
    }

    async function createTodo(inputValue, setInputValue) {
        await todoApi.createTodoTasks({
            text: inputValue, done: false
        })
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
        setInputValue('');
    }

    async function deleteTodo(id) {
        await todoApi.deleteTodoTask(id);
        const response = await todoApi.getTodoTasks();
        dispatch(initTodoTasks(response.data));
    }

    return {
        loadTodos,
        updateTodo,
        updateTaskText,
        createTodo,
        deleteTodo,
    };
}