import './todoStyle.css'
import {useNavigate} from "react-router-dom";

const DoneItem = (props) => {
    const {thing} = props
    const navigate = useNavigate();
    const handleTaskName = () => {
        navigate('/done/' + thing.id)
    }
    return (
        <div className='todoThing' onClick={handleTaskName}>
            <span>{thing.text}</span>
        </div>
    );
}

export default DoneItem;
