import { useSelector } from "react-redux";
import DoneItem from "./DoneItem";
import {List} from "antd";

const DoneGroup = () => {
    const doneList = useSelector((state) => state.todoList.todoList).filter(task => task.done);
    return (
        <div>
            <List
                header={<div>Done List</div>}
                bordered
                pagination={{
                    position: 'bottom',
                    align: 'end'
                }}
                dataSource={doneList}
                renderItem={(item) => (
                    <List.Item>
                        <DoneItem key={item.id} thing={item} />
                    </List.Item>
                )}
            />
        </div>
    );
}

export default DoneGroup;