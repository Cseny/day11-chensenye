import InputTodo from "./InputTodo";
import TodoList from "./TodoList";
import {useEffect} from "react";
import {useTodos} from "./hooks/useTodo";

const TodoListGenerator = () => {
    const {loadTodos} = useTodos();
    useEffect(() => {
        loadTodos()
    }, [loadTodos]);

    return (
        <div>
            <TodoList/>
            <InputTodo/>
        </div>
    );
}

export default TodoListGenerator;