import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";

const DoneDetail = () => {
    const { id } = useParams();
    const doneTask = useSelector((state) => state.todoList.todoList).find(task => task.id === id);
    return (
        <div className='done-detail'>
            <h1>Done Detail</h1>
            <div>{doneTask?.id}</div>
            <div>{doneTask?.text}</div>
        </div>
    );
}

export default DoneDetail;