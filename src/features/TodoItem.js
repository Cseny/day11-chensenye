import './todoStyle.css'
import { useTodos } from "./hooks/useTodo";
import { message, Modal, Popconfirm } from "antd";
import { useState } from "react";
import TextArea from "antd/es/input/TextArea";
import { DeleteOutlined, EditOutlined, EyeOutlined } from "@ant-design/icons";

const TodoItem = (props) => {
    const { task } = props
    const { updateTodo, deleteTodo, updateTaskText } = useTodos();
    const [isEditModalOpen, setIsEditModalOpen] = useState(false);
    const [textValue, setTextValue] = useState('');
    const info = () => {
        Modal.info({
            title: 'Todo info',
            content: (
                <div>
                    <p>id: {task.id}</p>
                    <p>text: {task.text}</p>
                    <p>done: {task.done ? 'true' : 'false'}</p>
                </div>
            ),
            onOk() { },
        });
    };
    const showEditModal = () => {
        setTextValue(task.text)
        setIsEditModalOpen(true);
    };
    const deleteTask = async () => {
        await deleteTodo(task.id);
        message.success('Click on Yes');
    };
    const updateDone = async () => {
        await updateTodo(task);
    };
    const handleOk = async () => {
        await updateTaskText(task.id, textValue)
        setIsEditModalOpen(false);
    };
    const handleCancel = () => {
        setTextValue('')
        setIsEditModalOpen(false);
    };
    const handleTextChange = (event) => {
        setTextValue(event.target.value);
    };
    return (
        <div className='todoThing'>
            <span className={task.done ? 'doneThing' : 'undoneThing'} onClick={updateDone}>{task.text}</span>
            <div className="removeButton">
                <EyeOutlined className='edit_icon' onClick={info} style={{ color: "blue" }} />
                <EditOutlined className='edit_icon' onClick={showEditModal} style={{ color: "blue" }} />
                <Modal title="Update Task" open={isEditModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <TextArea value={textValue} onChange={handleTextChange}></TextArea>
                </Modal>
                <Popconfirm
                    title="Delete the task"
                    description="Are you sure to delete this task?"
                    onConfirm={deleteTask}
                    okText="Yes"
                    cancelText="No"
                >
                    <DeleteOutlined style={{ color: "red" }} />
                </Popconfirm>
            </div>
        </div>
    );
}

export default TodoItem;
