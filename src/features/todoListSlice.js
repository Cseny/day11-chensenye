import {createSlice} from '@reduxjs/toolkit'

export const todoListSlice = createSlice({
    name: 'todoList',
    initialState: {
        todoList: []
    },
    reducers: {
        initTodoTasks: (state, action) => {
            state.todoList = action.payload;
        },
    }
})

export default todoListSlice.reducer
export const {initTodoTasks} = todoListSlice.actions