import TodoItem from "./TodoItem";
import {useSelector} from "react-redux";
import {List} from "antd";

const TodoList = () => {
    const todoList = useSelector((state) => state.todoList.todoList);
    return (
        <List
            header={<div>TodoList</div>}
            bordered
            pagination={{
                position: 'bottom',
                align: 'end'
            }}
            dataSource={todoList}
            renderItem={(item) => (
                <List.Item>
                    <TodoItem key={item.id} task={item}/>
                </List.Item>
            )}
        />
    );
}

export default TodoList;