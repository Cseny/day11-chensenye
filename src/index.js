import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import store from './app/store'
import {Provider} from 'react-redux'
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import ErrorPage from "./pages/ErrorPage";
import HelpPage from "./pages/HelpPage";
import DoneList from "./features/DoneList";
import TodoListGenerator from "./features/TodoListGenerator";
import DoneDetail from "./features/DoneDetail";

const root = ReactDOM.createRoot(document.getElementById('root'));
const router = createBrowserRouter([
    {
        path: "/",
        element: <App/>,
        errorElement: <ErrorPage/>,
        children: [
            {
                index: true,
                element: <TodoListGenerator/>
            },
            {
                path: "/done",
                element: <DoneList/>
            },
            {
                path:'/done/:id',
                element: <DoneDetail/>
            },
            {
                path: "/help",
                element: <HelpPage/>
            }
        ]
    },
]);
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <RouterProvider router={router}/>
        </Provider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
